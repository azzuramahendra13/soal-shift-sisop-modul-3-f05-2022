#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>

#define PORT 8080

void OJ_menu(int *sck){
    char OJ_command[20];

    while(1){
        printf("OJ Command: ");
        scanf("%s", OJ_command);
        send(*sck, OJ_command, strlen(OJ_command), 0);

        if(strstr(OJ_command, "add")){

        }else if(strstr(OJ_command, "see")){

        }else if(strstr(OJ_command, "download")){

        }else if(strstr(OJ_command, "submit")){

        }else if(strstr(OJ_command, "logout")){
            printf("Berhasil logout.\n\n");
            break;
        }
    }
    
}

int main() {
    // Client socket
    struct sockaddr_in adrs;
    int sck = 0; 
    int vread;
    int int_pton;
    struct sockaddr_in srv_adrs;

    sck = socket(AF_INET, SOCK_STREAM, 0);
    if(sck < 0){
        printf("Creation error\n");
        return -1;
    }
  
    memset(&srv_adrs, '0', sizeof(srv_adrs));
    srv_adrs.sin_family = AF_INET;
    srv_adrs.sin_port = htons(PORT);
    
    int_pton = inet_pton(AF_INET, "127.0.0.1", &srv_adrs.sin_addr);
    if(int_pton <= 0){
        printf("Invalid address or Address not supported\n");
        return -1;
    }
    
    if(connect(sck, (struct sockaddr *)&srv_adrs, sizeof(srv_adrs)) < 0){
        printf("Connection Failed \n");
        return -1;
    }


    // Mulai prosedur
    char *rgt = "register";
    char *lgn = "login";
    char *close_program = "close";
    char command[9] = "";
    char id[100] = "";
    char password[100] = "";
    
    while(1){
        printf("Command: ");
        scanf("%s", command);
        send(sck, command, strlen(command), 0);

        if(strcmp(command, rgt) == 0){
            char status[10];

            printf("Id: ");
            scanf("%s", id);
            send(sck, id, strlen(id), 0);

            printf("Password: ");
            scanf("%s", password);
            send(sck, password, strlen(password), 0);

            vread = read(sck, status, 10);

            if(strstr(status, "success")){
                printf("Berhasil register.\n\n");
            }else if(strstr(status, "fid")){
                printf("Username sudah terdaftar, harap coba yang lain.\n\n");
            }else if(strstr(status, "fpass")){
                printf("Password tidak sesuai ketentuan, harap coba yang lain.\n\n");
            }else if(strstr(status, "fail")){
                printf("Username dan Password tidak bisa dipakai, harap coba yang lain.\n\n");
            }

        }else if(strcmp(command, lgn) == 0){
            char status[10];

            printf("Id: ");
            scanf("%s", id);
            send(sck, id, strlen(id), 0);

            printf("Password: ");
            scanf("%s", password);
            send(sck, password, strlen(password), 0);

            vread = read(sck, status, 10);

            if(strstr(status, "success")){
                printf("Berhasil login.\n\n");
                OJ_menu(&sck);
            }else if(strstr(status, "fail")){
                printf("Tidak ada data.\n\n");
            }
        }else if(strcmp(command, close_program) == 0){
            break;
        }else{
            printf("Perintah tidak ada.\n\n");
        }
    }
    
    return 0;
}