#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <netinet/in.h>
#include <ctype.h>
#include <sys/socket.h>

#define PORT 8080

void main_menu(char current_client[], int *vread, int *n_sck){
    char OJ_command[100];

    while(1){
        *vread = read(*n_sck, OJ_command, 100);

        if(strstr(OJ_command, "add")){

        }else if(strstr(OJ_command, "see")){

        }else if(strstr(OJ_command, "download")){

        }else if(strstr(OJ_command, "submit")){

        }else if(strstr(OJ_command, "logout")){
            strcpy(current_client, "\0");
            break;
        }
    }
}

int main() {
    // Membuat file problems.tsv
    FILE *tsv;

    tsv = fopen("problems.tsv", "a+");
    fclose(tsv);
    

    // Server socket
    int srv_fd, n_sck, vread;
    struct sockaddr_in adrs;
    int opt = 1;
    int bnd;
    int lst;
    int addrlen = sizeof(adrs);
    
    srv_fd = socket(AF_INET, SOCK_STREAM, 0);
    if(srv_fd == 0){
        perror("Socket failed");
        exit(EXIT_FAILURE);
    }
      
    if(setsockopt(srv_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))){
        perror("Setsockopt");
        exit(EXIT_FAILURE);
    }

    adrs.sin_family = AF_INET;
    adrs.sin_addr.s_addr = INADDR_ANY;
    adrs.sin_port = htons(PORT);
    
    bnd = bind(srv_fd, (struct sockaddr *)&adrs, sizeof(adrs));
    if(bnd < 0){
        perror("Bind failed");
        exit(EXIT_FAILURE);
    }

    lst = listen(srv_fd, 3);
    if(lst < 0){
        perror("Listen");
        exit(EXIT_FAILURE);
    }

    n_sck = accept(srv_fd, (struct sockaddr *)&adrs, (socklen_t*)&addrlen);
    if(n_sck < 0){
        perror("Accept");
        exit(EXIT_FAILURE);
    }


    // Mulai prosedur

    char *rgt = "register";
    char *lgn = "login";
    char command[9] = "";
    char id[100] = "";
    char password[100] = "";
    char current_client[100] = "\0";

    while(1){
        vread = read(n_sck, command, 9);

        if(strcmp(command, rgt) == 0){
            FILE *fp = fopen("users.txt", "a+");
            char data[300], temp[100], to_be_written[300], status[8] = "success";
            int i, len;
            bool can_use_id = true, can_use_password = true;

            // Cek jika ada username sama
            vread = read(n_sck, id, 100);
            memset(temp, '\0', sizeof(temp));

            while(fgets(data, 300, fp)){
                i = 0;

                while(data[i] != ':'){
                    i++;
                }
                
                strncpy(temp, data, i);

                if(strcmp(temp, id) == 0){
                    can_use_id = false;
                    break;
                }
            }
            
            strcpy(to_be_written, id);
            strcat(to_be_written, ":");

            // Cek jika password bisa dipakai
            vread = read(n_sck, password, 100);
            len = strlen(password);

            if(len < 6) can_use_password = false;

            if(can_use_password){
                for(i = 0; i < len; i++){
                    if(isdigit(password[i])){
                        break;
                    }else if(i == len - 1){
                        can_use_password = false;
                    }
                }

                if(can_use_password){
                    for(i = 0; i < len; i++){
                        if(password[i] >= 65 && password[i] <= 90){
                            break;
                        }else if(i == len - 1){
                            can_use_password = false;
                        }
                    }

                    if(can_use_password){
                        for(i = 0; i < len; i++){
                            if(password[i] >= 97 && password[i] <= 122){
                                break;
                            }else if(i == len - 1){
                                can_use_password = false;
                            }
                        }
                    }
                }
            }

            strcat(to_be_written, password);

            if(can_use_id){
                if(can_use_password){
                    fprintf(fp, "%s\n", to_be_written);
                }else{
                    strcpy(status, "fpass");
                }
                
            }else{
                if(can_use_password){
                    strcpy(status, "fid");               
                }else{
                    strcpy(status, "fail");
                }
            }

            send(n_sck, status, strlen(status), 0);

            fclose(fp);

        }else if(strcmp(command, lgn) == 0){
            FILE *fp = fopen("users.txt", "r");
            char target[300], data[300], status[8] = "success";
            bool user_data_exist = false;


            vread = read(n_sck, id, 100);
            strcpy(target, id);

            strcat(target, ":");

            vread = read(n_sck, password, 100);
            strcat(target, password);
            strcat(target, "\n");

            while(fgets(data, 300, fp)){
                if(strcmp(data, target) == 0){
                    user_data_exist = true;
                    break;
                }
            }

            fclose(fp);

            if(user_data_exist){
                send(n_sck, status, strlen(status), 0);
                strcpy(current_client, id);
                main_menu(current_client, &vread, &n_sck);
            }else{
                strcpy(status, "fail");
                send(n_sck, status, strlen(status), 0);
            }
        }
    }
    
    return 0;
}