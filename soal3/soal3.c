#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>
#include <unistd.h>

//untuk mengecek apakah file ada
int checkexist(const char *filename)
{
    struct stat buf;
    int exist = stat(filename, &buf);
    if (exist == 0)
        return 1;
    else // -1
        return 0;
}

//fungsi work
void *work(void *filename)
{
    char file[100], existsfile[100], listdir[200], hidden[100], listhidden[100];
    int i;
    
    strcpy(existsfile, filename);
    strcpy(listhidden, filename);
    
    char *nama2 = strrchr(listhidden, '/');
    strcpy(hidden, nama2);

    //Jika suatu file hidden
    if (hidden[1] == '.') {
        strcpy(listdir, "Hidden");
    }
    
    //Untuk file yang tidak diketahui ekstensinya
    else if (strstr(filename, ".") == NULL) {
        strcpy(listdir, "Unknown");
    }
    
    //Untuk file normal
    else {
        strcpy(file, filename);
        strtok(file, ".");
        char *token = strtok(NULL, "");
        //karena tidak case sensitive
        for (i = 0; token[i]; i++) {
            token[i] = tolower(token[i]);
        }
        strcpy(listdir, token);
    }

    //Jika file belum ada, maka dibuatlah folder
    int exist = checkexist(existsfile);
    if (exist) mkdir(listdir, 0755);
    
    char cwd[PATH_MAX];

    //Untuk mendapatkan format nama1 file
    if (getcwd(cwd, sizeof(cwd)) != NULL)
    {
        char *nama1 = strrchr(filename, '/');
        char namafile[200];
        strcpy(namafile, cwd);
        strcat(namafile, "/");
        strcat(namafile, listdir);
        strcat(namafile, nama1);

        rename(filename, namafile);
    }
}

//Fungsi untuk merekursif tiap file
void listFilesRecursively(char *basePath)
{
	DIR *dir = opendir(basePath);
    char path[1000];
    struct dirent *dp;
    struct stat buffer;
//    int n = 0;

    if (!dir) return;

    while ((dp = readdir(dir)) != NULL) {
        if (strcmp(dp->d_name, "..") != 0 && strcmp(dp->d_name, ".") != 0) {
            //Membuat path baru
            strcpy(path, basePath);
            strcat(path, "/");
            strcat(path, dp->d_name);

            if (stat(path, &buffer) == 0 && S_ISREG(buffer.st_mode)) {
                //membuat thread untuk menjalankan work
                pthread_t thread;
                int err = pthread_create(&thread, NULL, work, (void *)path);
                pthread_join(thread, NULL);
            }
            
            //kondisi rekursif
			listFilesRecursively(path);
        }
    }
    closedir(dir);
}

//fungsi main
int main()
{
    char cwd[PATH_MAX];
    if (getcwd(cwd, sizeof(cwd)) != NULL)
    {
    //membuka working directory sendiri
    listFilesRecursively(cwd);
    }

}
