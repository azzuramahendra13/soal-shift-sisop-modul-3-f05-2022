#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <stdio.h>
#include <wait.h>
#include <stdlib.h>
#include <pthread.h>

pthread_t tid[6];
pid_t child_id;
int length = 5;

void createFolder()
{
    child_id = fork();
    int status;
  
    char *argv1[] = {"mkdir", "-p", "/home/wahyu/modul3/quote", NULL};
    char *argv2[] = {"mkdir", "-p", "/home/wahyu/modul3/music", NULL};
    char *argv3[] = {"mkdir", "-p", "/home/wahyu/modul3/hasil", NULL};
    
    if(child_id == 0)
    {
        execv("/usr/bin/mkdir", argv1);
    }
    else if(child_id == 1)
    {
        execv("/usr/bin/mkdir", argv3);
    }
    else
    {
        while(wait(&status) > 0);
        execv("/usr/bin/mkdir", argv2);
    }
}

void *threadUnzip(void *arg)
{
    char *argv1[] = {"unzip", "-q", "quote.zip", NULL};
    char *argv2[] = {"unzip", "-q", "music.zip", NULL};

    unsigned long i = 0;

    pthread_t id = pthread_self();
    int iter;

    if(pthread_equal(id, tid[0]))
    {
        child_id = fork();
        if(child_id == 0)
            execv("/usr/bin/unzip", argv1);
    }
    else if(pthread_equal(id, tid[1]))
    {
        child_id = fork();
        if(child_id == 0)
            execv("/usr/bin/unzip", argv2);
    }

    return NULL;
}

/*void *threadDecode(void *arg)
{
    char *argv1[] = {"unzip", "-q", "quote.zip", NULL};
    char *argv2[] = {"unzip", "-q", "music.zip", NULL};

    unsigned long i = 0;

    pthread_t id = pthread_self();
    int iter;

    if(pthread_equal(id, tid[0]))
    {
        child_id = fork();
        if(child_id == 0)
            execv("/usr/bin/unzip", argv1);
    }
    else if(pthread_equal(id, tid[1]))
    {
        child_id = fork();
        if(child_id == 0)
            execv("/usr/bin/unzip", argv2);
    }

    return NULL;
}

void moveFile(char *folder, char *source)
{
    char coreFolder1[] = "/home/wahyu/modul3/quote";
    char coreFolder2[] = "/home/wahyu/modul3/music";
    strcat(coreFolder1, folder);
    strcat(coreFolder2, folder);
    char *argv[] = {"mv", source, coreFolder1, coreFolder2, NULL};
    createProcess("/usr/bin/mv", argv);
}*/

int main()
{
    int err;
    int i = 0;
    while(i < 2)
    {
        err = pthread_create(&(tid[i]), NULL, &threadUnzip, NULL);
        
        if(err != 0)
        {
            printf("can't unzip : [%s]", strerror(err));
        }  
        else
        {
            printf("create thread succes");
        }
        i++;
    }
    createFolder();
    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);
    exit(0);
    
    return 0;
}
