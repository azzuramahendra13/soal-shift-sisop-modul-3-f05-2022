# Laporan Resmi Soal Shift Sistem Operasi Modul 3 F05 2022

Kelompok F05 :

                - Azzura Mahendra Putra Malinus     5025201211
                - Syaiful Bahri Dirgantara          5025201203
                - Wahyu Tri Saputro                 5025201217

## Soal 1
Soal meminta untuk mengekstract dua file zip menggunakan thread. Kemudian file yang sudah diekstract dimasukkan ke dalam dua folder berbeda sesuai nama file zip. Selanjutnya soal meminta untuk mendecode file .txt dari zip dan dipindah ke file .txt baru menggunakan thread. Lalu, file hasil decode dimasukkan ke folder bernama hasil yang nantinya akan di zip dan diberi password sesuai permintaan soal. Karena ada yang kurang, soal meminta untuk unzip file hasil.zip dan buat file no.txt dengan tulisan 'No' pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt menjadi hasil.zip, dengan password yang sama seperti sebelumnya.

### Pengerjaan Soal

#### 1. Membuat folder music dan quote

```C
pthread_t tid[6];
pid_t child_id;
int length = 5;
```

Pertama - tama kita buat dulu variabel global yang nanti akan di gunakan di beberapa fungsi di dalam code.

```C
void createFolder()
{
    child_id = fork();
    int status;
  
    char *argv1[] = {"mkdir", "-p", "/home/wahyu/modul3/quote", NULL};
    char *argv2[] = {"mkdir", "-p", "/home/wahyu/modul3/music", NULL};
    char *argv3[] = {"mkdir", "-p", "/home/wahyu/modul3/hasil", NULL};
    
    if(child_id == 0)
    {
        execv("/usr/bin/mkdir", argv1);
    }
    else if(child_id == 1)
    {
        execv("/usr/bin/mkdir", argv3);
    }
    else
    {
        while(wait(&status) > 0);
        execv("/usr/bin/mkdir", argv2);
    }
}
```

#### 2. Membuat thread untuk mengunzip quote.zip dan music.zip

```C
void *threadUnzip(void *arg)
{
    char *argv1[] = {"unzip", "-q", "quote.zip", NULL};
    char *argv2[] = {"unzip", "-q", "music.zip", NULL};

    unsigned long i = 0;

    pthread_t id = pthread_self();
    int iter;

    if(pthread_equal(id, tid[0]))
    {
        child_id = fork();
        if(child_id == 0)
            execv("/usr/bin/unzip", argv1);
    }
    else if(pthread_equal(id, tid[1]))
    {
        child_id = fork();
        if(child_id == 0)
            execv("/usr/bin/unzip", argv2);
    }

    return NULL;
}
```

Baru setelah itu, kita membuat fungsi untuk mengunzip dua file zip yang diminta oleh soal.

#### 3. Main function

Setelah kita membuat fungsi - fungsi tersebut, maka saatnya mengimplementasikannya ke dalam main function.

```C
int main()
{
    int err;
    int i = 0;
    while(i < 2)
    {
        err = pthread_create(&(tid[i]), NULL, &threadUnzip, NULL);
        
        if(err != 0)
        {
            printf("can't unzip : [%s]", strerror(err));
        }  
        else
        {
            printf("create thread succes");
        }
        i++;
    }
    createFolder();
    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);
    exit(0);
    
    return 0;
}
```

### Kendala dalam Pengerjaan

Mengalami stuck lama pada saat pengerjaan poin b dan seterusnya. Tidak bisa membuat folder padahal codingan sudah sesuai, bahkan mengimplementasikan codingan dari modul 2.

### Dokumentasi

#### 1. Hasil extract file quote.zip dan music.zip

## Soal 2
Soal meminta untuk membuat sebuah simulasi online judge. Fitur-fitur yang dimiliki oleh online judge tersebut antara lain melakukan register dan login, menambahkan soal (add), melihat soal (see), mengunduh soal (download), dan mengumpulkan jawaban (submit). Online judge yang akan dibuat akan berbentuk client-server, dengan server bisa menangani beberapa client sekaligus (jika terdapat sebuah client yang sedang login, client lain tidak bisa mengakses server). Model server-client ini akan dibuat menggunakan socket.

## Pengerjaan Soal
### Sisi Server
### Deklarasi Header dan Macro
```C
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <netinet/in.h>
#include <ctype.h>
#include <sys/socket.h>

#define PORT 8080
```
### Fungsi main_menu
Fungsi ini bertujuan untuk memberikan akses menu kepada client yang sudah login. Namun, implementasi fungsi ini masih belum selesai karena keterbatasan waktu dan pengetahuan.
```C
void main_menu(char current_client[], int *vread, int *n_sck){
    char OJ_command[100];

    while(1){
        *vread = read(*n_sck, OJ_command, 100);

        if(strstr(OJ_command, "add")){

        }else if(strstr(OJ_command, "see")){

        }else if(strstr(OJ_command, "download")){

        }else if(strstr(OJ_command, "submit")){

        }else if(strstr(OJ_command, "logout")){
            strcpy(current_client, "\0");
            break;
        }
    }
}
```
Client dapat mengakses fitur-fitur yang disediakan pada online judge setelah login, seperti yang ada pada kode di atas.
### Fungsi main
### Membuat file `problems.tsv` untuk menyimpan judul problem dan penulisnya
```C
    // Membuat file problems.tsv
    FILE *tsv;

    tsv = fopen("problems.tsv", "a+");
    fclose(tsv);
```
### Melakukan pengaturan socket untuk server, mengikuti modul Sistem Operasi yang diberikan sebelumnya
```C
    // Server socket
    int srv_fd, n_sck, vread;
    struct sockaddr_in adrs;
    int opt = 1;
    int bnd;
    int lst;
    int addrlen = sizeof(adrs);
    
    srv_fd = socket(AF_INET, SOCK_STREAM, 0);
    if(srv_fd == 0){
        perror("Socket failed");
        exit(EXIT_FAILURE);
    }
      
    if(setsockopt(srv_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))){
        perror("Setsockopt");
        exit(EXIT_FAILURE);
    }

    adrs.sin_family = AF_INET;
    adrs.sin_addr.s_addr = INADDR_ANY;
    adrs.sin_port = htons(PORT);
    
    bnd = bind(srv_fd, (struct sockaddr *)&adrs, sizeof(adrs));
    if(bnd < 0){
        perror("Bind failed");
        exit(EXIT_FAILURE);
    }

    lst = listen(srv_fd, 3);
    if(lst < 0){
        perror("Listen");
        exit(EXIT_FAILURE);
    }

    n_sck = accept(srv_fd, (struct sockaddr *)&adrs, (socklen_t*)&addrlen);
    if(n_sck < 0){
        perror("Accept");
        exit(EXIT_FAILURE);
    }
```
### Memulai prosedur
### Mendeklarasikan variabel yang dibutuhkan
```C
    // Mulai prosedur

    char *rgt = "register";
    char *lgn = "login";
    char command[9] = "";
    char id[100] = "";
    char password[100] = "";
    char current_client[100] = "\0";
```
### Meminta perintah dari client dan masuk ke percabangan yang sesuai
```C
    while(1){
        vread = read(n_sck, command, 9);

        if(strcmp(command, rgt) == 0){
            FILE *fp = fopen("users.txt", "a+");
            char data[300], temp[100], to_be_written[300], status[8] = "success";
            int i, len;
            bool can_use_id = true, can_use_password = true;

            // Cek jika ada username sama
            vread = read(n_sck, id, 100);
            memset(temp, '\0', sizeof(temp));

            while(fgets(data, 300, fp)){
                i = 0;

                while(data[i] != ':'){
                    i++;
                }
                
                strncpy(temp, data, i);

                if(strcmp(temp, id) == 0){
                    can_use_id = false;
                    break;
                }
            }
            
            strcpy(to_be_written, id);
            strcat(to_be_written, ":");

            // Cek jika password bisa dipakai
            vread = read(n_sck, password, 100);
            len = strlen(password);

            if(len < 6) can_use_password = false;

            if(can_use_password){
                for(i = 0; i < len; i++){
                    if(isdigit(password[i])){
                        break;
                    }else if(i == len - 1){
                        can_use_password = false;
                    }
                }

                if(can_use_password){
                    for(i = 0; i < len; i++){
                        if(password[i] >= 65 && password[i] <= 90){
                            break;
                        }else if(i == len - 1){
                            can_use_password = false;
                        }
                    }

                    if(can_use_password){
                        for(i = 0; i < len; i++){
                            if(password[i] >= 97 && password[i] <= 122){
                                break;
                            }else if(i == len - 1){
                                can_use_password = false;
                            }
                        }
                    }
                }
            }

            strcat(to_be_written, password);

            if(can_use_id){
                if(can_use_password){
                    fprintf(fp, "%s\n", to_be_written);
                }else{
                    strcpy(status, "fpass");
                }
                
            }else{
                if(can_use_password){
                    strcpy(status, "fid");               
                }else{
                    strcpy(status, "fail");
                }
            }

            send(n_sck, status, strlen(status), 0);

            fclose(fp);

        }else if(strcmp(command, lgn) == 0){
            FILE *fp = fopen("users.txt", "r");
            char target[300], data[300], status[8] = "success";
            bool user_data_exist = false;


            vread = read(n_sck, id, 100);
            strcpy(target, id);

            strcat(target, ":");

            vread = read(n_sck, password, 100);
            strcat(target, password);
            strcat(target, "\n");

            while(fgets(data, 300, fp)){
                if(strcmp(data, target) == 0){
                    user_data_exist = true;
                    break;
                }
            }

            fclose(fp);

            if(user_data_exist){
                send(n_sck, status, strlen(status), 0);
                strcpy(current_client, id);
                main_menu(current_client, &vread, &n_sck);
            }else{
                strcpy(status, "fail");
                send(n_sck, status, strlen(status), 0);
            }
        }
    }
    
    return 0;
```
### Penjelasan percabangan perintah register (rgt)
### Deklarasi variabel lokal
```C
            FILE *fp = fopen("users.txt", "a+");
            char data[300], temp[100], to_be_written[300], status[8] = "success";
            int i, len;
            bool can_use_id = true, can_use_password = true;
```
### Mengecek username yang diinginkan client, jika sudah ada yang terdaftar maka ditolah
```C
            // Cek jika ada username sama
            vread = read(n_sck, id, 100);
            memset(temp, '\0', sizeof(temp));

            while(fgets(data, 300, fp)){
                i = 0;

                while(data[i] != ':'){
                    i++;
                }
                
                strncpy(temp, data, i);

                if(strcmp(temp, id) == 0){
                    can_use_id = false;
                    break;
                }
            }
            
            strcpy(to_be_written, id);
            strcat(to_be_written, ":");
```
### Mengecek jika password yang diinginkan client, jika sesuai ketentuan maka diterima (lebih dari 6 karakter, terdiri dari angka, huruf besar, dan huruf kecil)
```C
            // Cek jika password bisa dipakai
            vread = read(n_sck, password, 100);
            len = strlen(password);

            if(len < 6) can_use_password = false;

            if(can_use_password){
                for(i = 0; i < len; i++){
                    if(isdigit(password[i])){
                        break;
                    }else if(i == len - 1){
                        can_use_password = false;
                    }
                }

                if(can_use_password){
                    for(i = 0; i < len; i++){
                        if(password[i] >= 65 && password[i] <= 90){
                            break;
                        }else if(i == len - 1){
                            can_use_password = false;
                        }
                    }

                    if(can_use_password){
                        for(i = 0; i < len; i++){
                            if(password[i] >= 97 && password[i] <= 122){
                                break;
                            }else if(i == len - 1){
                                can_use_password = false;
                            }
                        }
                    }
                }
            }

            strcat(to_be_written, password);
```
### Mengirim status register ke client, success jika berhasil, fpass jika password tidak bisa, fid jika username sudah terdaftar, dan fail untuk username dan password tidak bisa digunakan
```C
            if(can_use_id){
                if(can_use_password){
                    fprintf(fp, "%s\n", to_be_written);
                }else{
                    strcpy(status, "fpass");
                }
                
            }else{
                if(can_use_password){
                    strcpy(status, "fid");               
                }else{
                    strcpy(status, "fail");
                }
            }

            send(n_sck, status, strlen(status), 0);

            fclose(fp);
```
### Penjelasan percabangan perintah login (lgn)
### Deklarasi variabel lokal
```C
            FILE *fp = fopen("users.txt", "r");
            char target[300], data[300], status[8] = "success";
            bool user_data_exist = false;
```
### Meminta username dan password client
```C
            vread = read(n_sck, id, 100);
            strcpy(target, id);

            strcat(target, ":");

            vread = read(n_sck, password, 100);
            strcat(target, password);
            strcat(target, "\n");

            while(fgets(data, 300, fp)){
                if(strcmp(data, target) == 0){
                    user_data_exist = true;
                    break;
                }
            }

            fclose(fp);
```
### Mengirim status login ke client, success jika berhasil dan fail jika gagal (data tidak terdaftar)
```C
            if(user_data_exist){
                send(n_sck, status, strlen(status), 0);
                strcpy(current_client, id);
                main_menu(current_client, &vread, &n_sck);
            }else{
                strcpy(status, "fail");
                send(n_sck, status, strlen(status), 0);
            }
```
Jika client berhasil login, client akan diarahkan ke main menu online judge.

### Sisi Client
### Deklarasi header dan macro
```C
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>

#define PORT 8080
```
### Fungsi OJ_menu untuk mengakses menu online judge bagi client yang telah login, fungsi belum sepenuhnya diimplementasikan karena keterbatasan waktu dan pengetahuan
```C
void OJ_menu(int *sck){
    char OJ_command[20];

    while(1){
        printf("OJ Command: ");
        scanf("%s", OJ_command);
        send(*sck, OJ_command, strlen(OJ_command), 0);

        if(strstr(OJ_command, "add")){

        }else if(strstr(OJ_command, "see")){

        }else if(strstr(OJ_command, "download")){

        }else if(strstr(OJ_command, "submit")){

        }else if(strstr(OJ_command, "logout")){
            printf("Berhasil logout.\n\n");
            break;
        }
    }
    
}
```
### Fungsi main
### Melakukan pengaturen socket untuk client, diambil dari modul Sistem Operasi
```C
    // Client socket
    struct sockaddr_in adrs;
    int sck = 0; 
    int vread;
    int int_pton;
    struct sockaddr_in srv_adrs;

    sck = socket(AF_INET, SOCK_STREAM, 0);
    if(sck < 0){
        printf("Creation error\n");
        return -1;
    }
  
    memset(&srv_adrs, '0', sizeof(srv_adrs));
    srv_adrs.sin_family = AF_INET;
    srv_adrs.sin_port = htons(PORT);
    
    int_pton = inet_pton(AF_INET, "127.0.0.1", &srv_adrs.sin_addr);
    if(int_pton <= 0){
        printf("Invalid address or Address not supported\n");
        return -1;
    }
    
    if(connect(sck, (struct sockaddr *)&srv_adrs, sizeof(srv_adrs)) < 0){
        printf("Connection Failed \n");
        return -1;
    }
```
### Memulai prosedur
### Deklarasi variabel
```C
    char *rgt = "register";
    char *lgn = "login";
    char *close_program = "close";
    char command[9] = "";
    char id[100] = "";
    char password[100] = "";
```
### Meminta perintah dari client dan masuk ke percabangan yang sesuai
```C
    while(1){
        printf("Command: ");
        scanf("%s", command);
        send(sck, command, strlen(command), 0);

        if(strcmp(command, rgt) == 0){
            char status[10];

            printf("Id: ");
            scanf("%s", id);
            send(sck, id, strlen(id), 0);

            printf("Password: ");
            scanf("%s", password);
            send(sck, password, strlen(password), 0);

            vread = read(sck, status, 10);

            if(strstr(status, "success")){
                printf("Berhasil register.\n\n");
            }else if(strstr(status, "fid")){
                printf("Username sudah terdaftar, harap coba yang lain.\n\n");
            }else if(strstr(status, "fpass")){
                printf("Password tidak sesuai ketentuan, harap coba yang lain.\n\n");
            }else if(strstr(status, "fail")){
                printf("Username dan Password tidak bisa dipakai, harap coba yang lain.\n\n");
            }

        }else if(strcmp(command, lgn) == 0){
            char status[10];

            printf("Id: ");
            scanf("%s", id);
            send(sck, id, strlen(id), 0);

            printf("Password: ");
            scanf("%s", password);
            send(sck, password, strlen(password), 0);

            vread = read(sck, status, 10);

            if(strstr(status, "success")){
                printf("Berhasil login.\n\n");
                OJ_menu(&sck);
            }else if(strstr(status, "fail")){
                printf("Tidak ada data.\n\n");
            }
        }else if(strcmp(command, close_program) == 0){
            break;
        }else{
            printf("Perintah tidak ada.\n\n");
        }
    }
```
### Penjelasan percabangan register
### Mengirim username dan password yang diinputkan client ke server
```C
            char status[10];

            printf("Id: ");
            scanf("%s", id);
            send(sck, id, strlen(id), 0);

            printf("Password: ");
            scanf("%s", password);
            send(sck, password, strlen(password), 0);
```
### Menerima status dari server
```C
            vread = read(sck, status, 10);

            if(strstr(status, "success")){
                printf("Berhasil register.\n\n");
            }else if(strstr(status, "fid")){
                printf("Username sudah terdaftar, harap coba yang lain.\n\n");
            }else if(strstr(status, "fpass")){
                printf("Password tidak sesuai ketentuan, harap coba yang lain.\n\n");
            }else if(strstr(status, "fail")){
                printf("Username dan Password tidak bisa dipakai, harap coba yang lain.\n\n");
            }
```
### Penjelasan percabangan login
### Mengirim username dan password ke server, dan menerima status dari server
```C
            char status[10];

            printf("Id: ");
            scanf("%s", id);
            send(sck, id, strlen(id), 0);

            printf("Password: ");
            scanf("%s", password);
            send(sck, password, strlen(password), 0);

            vread = read(sck, status, 10);

            if(strstr(status, "success")){
                printf("Berhasil login.\n\n");
                OJ_menu(&sck);
            }else if(strstr(status, "fail")){
                printf("Tidak ada data.\n\n");
            }
```
Jika status success, maka client akan dibawa ke menu online judge.
### Kendala dalam Pengerjaan
Dalam pengerjaan soal ini, ditemui kendala berupa ketidakkonsistenan nilai yang dikirimkan dari client ke server atau sebaliknya sehingga menyebabkan program tidak berjalan dengan benar. Selain itu, masih ditemui kendala dalam menerapkan sistem multiclient sehingga program yang dibuat masih hanya mampu menangani satu client saja.
### Dokumentasi
#### 1. Struktur file dan folder
![dokum_2.1](Dokumentasi/soal2/Screenshot_from_2022-04-17_22-05-03.png)
#### 2. Tampilan menu awal
![dokum_2.2](Dokumentasi/soal2/Screenshot_from_2022-04-17_22-06-47.png)
#### 3. Isi users.txt
![dokum_2.3](Dokumentasi/soal2/Screenshot_from_2022-04-17_22-08-41.png)

## Soal 3

Soal ini pertama meminta untuk mengextract sebuah file bernama `hartakarun.zip` ke dalam directory `/home/[user]/shift3/`. Kemudian program diminta mengkategorikan file-file yang ada ke dalam directory-directory yang bernama ekstensinya secara rekursif perfile dengan tiap file dioperasikan oleh thread yang berbeda. Kemudian setelah dirapikan, semua harus di zip kembali dengan nama `hartakarun.zip` ke directory client untuk kemudian dikirimkan ke directory server dengan command `send hartakarun.zip`.

### Pengerjaan Soal

#### 1. Mengunzip hartakarun.zip
```bash
unzip hartakarun.zip -d shift3/ && cd shift3/hartakarun/
```

#### 2. Deklarasi Header

```C
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>
#include <unistd.h>
```

#### 3. Fungsi checkexist()

Fungsi yang berfungsi untuk mengecek apakah sebuah file itu ada atau tidak. Parameternya berupa file dan return type nya berupa integer.

```C
//untuk mengecek apakah file ada
int checkexist(const char *filename)
{
    struct stat buf;
    int exist = stat(filename, &buf);
    if (exist == 0)
        return 1;
    else // -1
        return 0;
}
```

#### 4. Fungsi work()

Fungsi utama yang berfungsi mengidentifikasi tiap file dengan mencari ekstensinya dan memasukkannya ke dalam list hingga kemudian dibuat directory dan file tersebut dimasukkan ke dalamnya.

```C
//fungsi work
void *work(void *filename)
{
    char file[100], existsfile[100], listdir[200], hidden[100], listhidden[100];
    int i;
    
    strcpy(existsfile, filename);
    strcpy(listhidden, filename);
    
    char *nama2 = strrchr(listhidden, '/');
    strcpy(hidden, nama2);

    //Jika suatu file hidden
    if (hidden[1] == '.') {
        strcpy(listdir, "Hidden");
    }
    
    //Untuk file yang tidak diketahui ekstensinya
    else if (strstr(filename, ".") == NULL) {
        strcpy(listdir, "Unknown");
    }
    
    //Untuk file normal
    else {
        strcpy(file, filename);
        strtok(file, ".");
        char *token = strtok(NULL, "");
        //karena tidak case sensitive
        for (i = 0; token[i]; i++) {
            token[i] = tolower(token[i]);
        }
        strcpy(listdir, token);
    }

    //Jika file belum ada, maka dibuatlah folder
    int exist = checkexist(existsfile);
    if (exist) mkdir(listdir, 0755);
    
    char cwd[PATH_MAX];

    //Untuk mendapatkan format nama1 file
    if (getcwd(cwd, sizeof(cwd)) != NULL)
    {
        char *nama1 = strrchr(filename, '/');
        char namafile[200];
        strcpy(namafile, cwd);
        strcat(namafile, "/");
        strcat(namafile, listdir);
        strcat(namafile, nama1);

        rename(filename, namafile);
    }
}
```

#### 5. Fungsi listFilesRecursively()

Fungsi yang digunakan untuk membuat program berjalan secara rekursif di tiap file dan tiap file ditangani oleh 1 thread.

```C
//Fungsi untuk merekursif tiap file
void listFilesRecursively(char *basePath)
{
	DIR *dir = opendir(basePath);
    char path[1000];
    struct dirent *dp;
    struct stat buffer;
//    int n = 0;

    if (!dir) return;

    while ((dp = readdir(dir)) != NULL) {
        if (strcmp(dp->d_name, "..") != 0 && strcmp(dp->d_name, ".") != 0) {
            //Membuat path baru
            strcpy(path, basePath);
            strcat(path, "/");
            strcat(path, dp->d_name);

            if (stat(path, &buffer) == 0 && S_ISREG(buffer.st_mode)) {
                //membuat thread untuk menjalankan work
                pthread_t thread;
                int err = pthread_create(&thread, NULL, work, (void *)path);
                pthread_join(thread, NULL);
            }
            
            //kondisi rekursif
			listFilesRecursively(path);
        }
    }
    closedir(dir);
}
```

#### 6. Fungsi main

Fungsi main disini hanya berisi menangkap working directory saat ini dan menjadikannya paramater saat menjalankan fungsi `listFilesRecursively()`.

```C
//fungsi main
int main()
{
    char cwd[PATH_MAX];
    if (getcwd(cwd, sizeof(cwd)) != NULL)
    {
    //membuka working directory sendiri
    listFilesRecursively(cwd);
    }

}
```

### Kendala dalam Pengerjaan

Pada poin 3.d dan 3.e belum selesai dikerjakan karena terdapat kendala, ketika pengimplementasikan ke koding ada beberapa biyte data yang hilang sehingga tidak dapat terkirim dengan sempurna. Kemudian ketika revisi mencoba dengan mengirim file secara binary masih stuck karena melihat referensi yang mengirim filenya dari server ke client, dan ketika dicoba dibalik masih belum bisa terkirim dengan sempurna juga filenya.

### Dokumentasi
#### Poin 3.a : mengunzip hartakarun.zip
![dokum_3.1](Dokumentasi/soal3/3.a.1.jpg)
#### Poin 3.a, 3.b, 3.c : mengkategorikan file-file ke dalam directory-directory
![dokum_3.2](Dokumentasi/soal3/3.a.2.jpg)
![dokum_3.3](Dokumentasi/soal3/3.a.3.jpg)
